package com.girnarsoft.example.jdbc;


import java.util.ArrayList;
import java.util.HashMap;
/**
 * 
 * @author gspl
 * Some extra functionalities are implemented of CEO
 */

public class CEOService extends HRService{


	/**
	 * 
	 * @param promoteId
	 * @param selfId
	 * CEO can promote employees to manager and manager to Hr
	 */
	public void promoteEmployee(int promoteId, int selfId) {
		try {
			int employeeRole =JdbcConnector.getRole(promoteId);
			JdbcConnector.promoteEmployee(promoteId, employeeRole);

		}catch(Exception e) {
			e.getMessage();
		}
	}


	/**
	 * 
	 * @param selfId
	 * @param employeeId
	 * CEO can remove any employee irrespective of its role
	 */
	public void removeEmployee(int employeeId,int selfId)
	{
		try {
			//JdbcConnector.deleteEmployee(employeeId);
			int head=JdbcConnector.getHead(employeeId);
			JdbcConnector.updateHead(employeeId,head);

		}catch (Exception e) {
			e.getMessage();
		}

	}




}

