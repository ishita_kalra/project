package com.girnarsoft.example.jdbc;

import java.util.*;
/**
 * 
 * @author gspl
 * Some extra functionalities than that of manager are implemented 
 */

public class HRService extends ManagerService{


	/**
	 * 
	 * @param promoteId
	 * @param selfId
	 * Hr can promote employees to manager only
	 */
	public void promoteEmployee(int promoteId,int selfId) {

		try {
			int employeeRole =JdbcConnector.getRole(promoteId);
			if(employeeRole<1)
			{
				JdbcConnector.promoteEmployee(promoteId, employeeRole);
			}else {
				System.out.println("Invalid Operation");
			}
		}catch(Exception e) {
			e.getMessage();
		}
	}

	/**
	 * 
	 * @param selfId
	 * @param employeeId
	 * Hr can remove employees and managers
	 */
	public void removeEmployee(int employeeId,int selfId)
	{
		try {
			if(JdbcConnector.getRole(employeeId) <= 1)
			{
				JdbcConnector.deleteEmployee(employeeId);
				int head=JdbcConnector.getHead(employeeId);
				JdbcConnector.updateHead(employeeId,head);

			}
			else
			{
				System.out.println("Invalid Operation");
			}
		}catch (Exception e) {
			e.getMessage();
		}

	}


}

