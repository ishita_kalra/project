package com.girnarsoft.example.jdbc;

import java.sql.SQLException;
import java.util.*;

/**
 * function calls according to the option selected by user function call of the
 * class according to the role of the user CEO being given highest authorities
 * followed by HR.
 */

public class Execution {
	public static final Scanner SCANNER = new Scanner(System.in);
	private static int SELF_ID;
	private static String STRING_SELF_ID;
	private static String STRING_EMPLOYEE_ID;
	private static int EMPLOYEE_ID;
	private static int selfRole;
	private static EmployeeService employeeService;
	private static ManagerService managerService;
	private static HRService hrService;
	private static CEOService ceoService;

	public static void main(String[] args) {
		int userInput = 0;

		do {

			System.out.println("Press 1 to print all employees");
			System.out.println("press 2 to remove an employee");
			System.out.println("press 3 to add a new employee");
			System.out.println("press 4 to promote an employee");
			System.out.println("press 5 to find the list of reporting employees");
			System.out.println("press 6 to find the head id");
			System.out.println("press 7 to exit");
			String input = SCANNER.nextLine();
			try {

				while (!isInteger(input) || Integer.parseInt(input) > 7 || Integer.parseInt(input) < 1) {
					System.out.println("Please enter in correct format and within range");
					input = SCANNER.nextLine();
				}

				userInput = Integer.parseInt(input);
			} catch (Exception e) {
				System.out.println("Please enter in correct format and within range");
			}
			;
			switch (userInput) {
			case 1:
				printAllEmployees();
				break;
			case 2:
				takeInputRemove();
				break;
			case 3:
				takeAllDetails();
				System.out.println("Employee is successfully Added!!!");
				break;
			case 4:
				takePromoteDetails();
				break;
			case 5:
				listReportingEmployees();
				break;
			case 6:
				findManager();
				break;
			default:
				break;

			}
		} while (userInput != 7);
		SCANNER.close();
	}

	/**
	 * function to check if string having alphabets and whitespaces
	 * 
	 * @param name
	 * @return
	 */

	public static boolean isAlpha(String name) {
		return name.matches("[a-zA-Z][a-zA-Z ]*");
	}

	/**
	 * check if salary is in correct format
	 * 
	 * @param salary
	 * @return
	 */

	public static boolean validateFloat(String salary) {
		if (salary.matches("\\d*")) {
			return true;
		} else
			return false;
	}

	/**
	 * check if phone number is in correct format
	 * 
	 * @param phone
	 * @return
	 */

	public static boolean validatePhone(String phone) {
		if (phone.matches("\\d*") && phone.length() == 10) {
			return true;
		} else
			return false;
	}

	/**
	 * 
	 * @param input
	 * @return
	 */

	public static boolean isInteger(String input) {
		return (input.matches("^\\d+(\\.\\d+)?"));

	}

	/**
	 * Function to print all employees basic info
	 */
	public static void printAllEmployees() {
		try {
			JdbcConnector.viewEmployees();
		} catch (SQLException e) {
			e.getMessage();
		}

	}

	/**
	 * 
	 * @param string
	 * @return
	 */

	public static String validId(String string) {
		while (!isInteger(string)) {
			System.out.println("Please Enter the correct Id");
			string = SCANNER.nextLine();
		}
		return string;
	}

	/**
	 * validating users id and password and then performing the required action
	 */
	public static void validateUser() {

		System.out.println("Enter your id");
		STRING_SELF_ID = validId(SCANNER.nextLine());
		SELF_ID = Integer.parseInt(STRING_SELF_ID);

		System.out.println("Enter your password");
		String selfPassword = SCANNER.nextLine();
		try {
			while (!JdbcConnector.validateUser(SELF_ID, selfPassword)) {
				System.out.println("Wrong Password");
				System.out.println("Enter your password");
				selfPassword = SCANNER.nextLine();
			}
			System.out.println("correct password!!!");
			selfRole = JdbcConnector.getRole(SELF_ID);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public static void takeInputs() {
		validateUser();
		
		switch (selfRole) {
		case 0:
			employeeService = new EmployeeService();
			break;
		case 1:
			managerService = new ManagerService();
			break;
		case 2:
			hrService = new HRService();
			break;
		case 3:
			ceoService = new CEOService();
			break;
		default:
			System.out.println("Invalid Input");
		}

	}

	/**
	 * Calling the function of appropriate class according to the role of the user
	 */

	public static void takeInputAdd() {
		takeInputs();
		switch (selfRole) {
		case 0:
			employeeService.addEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 1:
			managerService.addEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 2:
			hrService.addEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 3:
			ceoService.addEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		default:
			System.out.println("Invalid Input");
		}

	}

	/**
	 * function call to remove an employee based on your designation
	 */

	public static void takeInputRemove() {
		takeInputs();
		System.out.println("Enter the id of the employee to be removed");

		STRING_EMPLOYEE_ID = validId(SCANNER.nextLine());
		EMPLOYEE_ID = Integer.parseInt(STRING_EMPLOYEE_ID);
		switch (selfRole) {
		case 0:
			employeeService.removeEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 1:
			managerService.removeEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 2:
			hrService.removeEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 3:
			ceoService.removeEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		default:
			System.out.println("Invalid Input");
		}

	}

	/**
	 * Function to input details of new employee and adding it
	 */

	public static void takeAllDetails() {
		String name;
		int role;
		String inputRole;
		String headId;

		System.out.println("Enter Name of the Employee");
		name = SCANNER.nextLine();
		while (!isAlpha(name)) {
			System.out.println("Please enter name in correct format");
			name = SCANNER.nextLine();
		}
		System.out.println("Enter mobile number");
		String stringPhone = SCANNER.nextLine();
		while (!validatePhone(stringPhone)) {
			System.out.println("Enter mobile number in correct format");
			stringPhone = SCANNER.nextLine();
		}
		long phone = Long.parseLong(stringPhone);

		System.out.println("Enter Salary of the employee");
		String stringSalary = SCANNER.nextLine();
		while (!validateFloat(stringSalary)) {
			System.out.println("Enter Salary in correct format");
			stringSalary = SCANNER.nextLine();
		}
		float salary = Float.parseFloat(stringSalary);

		System.out.println("Enter its Role in numerical format");
		System.out.println("0 : Employee");
		System.out.println("1 : Manager");
		System.out.println("2 : HR");
		inputRole = SCANNER.nextLine();
		inputRole = SCANNER.nextLine();
		while (!isInteger(inputRole) || Integer.parseInt(inputRole) > 2 || Integer.parseInt(inputRole) < 0) {
			System.out.println("Please enter role in correct Format");
			System.out.println("0 : Employee");
			System.out.println("1 : Manager");
			System.out.println("2 : HR");
			inputRole = SCANNER.nextLine();
		}
		role = Integer.parseInt(inputRole);

		System.out.println("Enter the id of the head");
		try {
			JdbcConnector.viewEmployeesId();
		}catch (Exception e) {
			e.getMessage();
		}
		headId = validId(SCANNER.nextLine());
		int head = Integer.parseInt(headId);
		
		System.out.println("Enter the password of the employee");
		String password = SCANNER.nextLine();

		try {
			JdbcConnector.addEmployee(name, phone, salary, password, role, head);
		} catch (Exception e) {
			e.getMessage();
		}

	}

	/**
	 * Input details through the method takeInput() Function call according to the
	 * designation of the employee
	 */

	public static void takePromoteDetails() {
		takeInputs();
		System.out.println("Enter the id of the employee to be promoted");

		STRING_EMPLOYEE_ID = validId(SCANNER.nextLine());
		EMPLOYEE_ID = Integer.parseInt(STRING_EMPLOYEE_ID);

		switch (selfRole) {
		case 0:
			employeeService.promoteEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 1:
			managerService.promoteEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 2:
			hrService.promoteEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		case 3:
			ceoService.promoteEmployee(EMPLOYEE_ID, SELF_ID);
			break;
		default:
			System.out.println("Invalid Input");
		}
	}

	/**
	 * Lists all the reporting employees of the person
	 */
	public static void listReportingEmployees() {
		System.out.println("Enter your id");
		STRING_SELF_ID = validId(SCANNER.nextLine());
		SELF_ID = Integer.parseInt(STRING_SELF_ID);

		System.out.println("Enter your password");
		String selfPassword = SCANNER.nextLine();
		try {
			while (!JdbcConnector.validateUser(SELF_ID, selfPassword)) {
				System.out.println("Wrong Password");
				System.out.println("Enter your password");
				selfPassword = SCANNER.nextLine();
			}
			System.out.println("correct password!!!");
			JdbcConnector.listReportingEmployees(SELF_ID);

		} catch (Exception e) {
			e.getMessage();
		}
	}

	/**
	 * Lists the head of the employee
	 */
	public static void findManager() {
		try {
			validateUser();
		int head=JdbcConnector.getHead(SELF_ID);
		String headName=JdbcConnector.findEmployee(head);
		System.out.println("Head Id:"+head+" Head Name:"+headName);
		}catch(Exception e) {
			e.getMessage();
		}
	}

}
