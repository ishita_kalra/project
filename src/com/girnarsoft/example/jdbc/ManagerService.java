package com.girnarsoft.example.jdbc;

import java.util.*;
/**
 * 
 * @author gspl
 * some functionalities which are similar to that of employee are not overridden
 *
 */


public class ManagerService extends EmployeeService{
	

	/**
	 * 
	 * @param selfId
	 * @param employeeId
	 * Manager can only remove employees
	 */
	public void removeEmployee(int employeeId,int selfId)
	{
		try {
			if(JdbcConnector.getRole(employeeId) == 0)
			{
				JdbcConnector.deleteEmployee(employeeId);
			}
			else
			{
				System.out.println("Invalid Operation");
			}
			}catch (Exception e) {
					e.getMessage();
			}
		
		

	}
	

}

