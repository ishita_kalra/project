package com.girnarsoft.example.jdbc;

import java.sql.Connection;
import java.util.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class JdbcConnector {

	public static String JDBC_DRIVER ;
	public static String TARGET_DB_URL;
	public static String OUTPUT_FOLDER_PATH ;
	public static String INFORMATION_DB_URL ;
	public static String USER ;
	public static String PASS ;
	static Connection connection = null;
	static  Scanner scanner=new Scanner(System.in);
	public static ResourceBundle bundle = null;
	static {

		bundle=  ResourceBundle.getBundle("db");
		JDBC_DRIVER = bundle.getString("JDBC_DRIVER");
		TARGET_DB_URL = bundle.getString("TARGET_DB_URL");
		USER = bundle.getString("USER");
		PASS = bundle.getString("PASS");


		try{

			connection = getDbTxConnection(TARGET_DB_URL, USER, PASS);
		}catch(Exception e){

			e.printStackTrace();
		}
	}

	public static Connection getDbTxConnection(String dbUrl, String username, String password){
		Connection conn = null;
		try{
			Class.forName(JDBC_DRIVER);
			System.out.println("Connecting to database... ");
			conn = DriverManager.getConnection(dbUrl,username,password);
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}

	public static void viewEmployees() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.VIEW_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			rs=pstmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(2));
			}
			
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}



	}
	public static void viewEmployeesId() throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.VIEW_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			rs=pstmt.executeQuery();
			System.out.println("Available Employees are:");
			while(rs.next())
			{
				System.out.println(rs.getInt(1));
			}
			
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}



	}
	public static int getRole(int selfid) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;
		int ans=4;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.FIND_ROLE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, selfid);
			rs=pstmt.executeQuery();
			if(rs.next())
				ans=rs.getInt(1);
			return ans;
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
		return ans;

	}
	public static boolean validateUser(int selfid,String selfPassword) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			String password="";
			pstmt = connection.prepareStatement(Constants.Queries.VALIDATE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, selfid);
			rs=pstmt.executeQuery();
			if(rs.next())
				password=rs.getString(1);
			if(password.equals(selfPassword))
				return true;
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
		return false;

	}
	public static boolean deleteEmployee(int employee_id) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.REMOVE_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, employee_id);
			int numero=pstmt.executeUpdate();
			rs=pstmt.getGeneratedKeys();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
		return false;

	}
	public static boolean addEmployee(String name,long mobile,float salary,String password,int role,int manager_id) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.INSERT_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, name);
			pstmt.setLong(2, mobile);
			pstmt.setFloat(3, salary);
			pstmt.setString(4, password);
			pstmt.setInt(5, role);
			pstmt.setInt(6, manager_id);
			int numero=pstmt.executeUpdate();
			rs=pstmt.getGeneratedKeys();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
		return false;

	}
	public static void promoteEmployee(int employeeId,int employeeRole) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.UPGRADE_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, employeeRole+1);
			pstmt.setInt(2, employeeId);
			int numero=pstmt.executeUpdate();
			rs=pstmt.getGeneratedKeys();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
		

	}
	public static int getHead(int employeeId) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.VIEW_MANAGER, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, employeeId);
			rs=pstmt.executeQuery();
			if(rs.next())
			{
				return rs.getInt(1);
			}
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
		return 0;
	}
	public static void updateHead(int oldHead,int newHead) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.UPDATE_HEAD, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, newHead);
			pstmt.setInt(2, oldHead);
			int numero=pstmt.executeUpdate();
			rs=pstmt.getGeneratedKeys();
			//rs.next();
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
	}
	public static void listReportingEmployees(int employeeId) throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.LIST_REPORTING_EMPLOYEES, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, employeeId);
			rs=pstmt.executeQuery();
			while(rs.next())
			{
				System.out.println("Employee Id:"+rs.getInt(1) + " Name "+rs.getString(2));
			}
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
	}
	public static String findEmployee(int employee_id)throws SQLException {
		PreparedStatement pstmt = null ;
		ResultSet rs = null ;

		try{
			pstmt = connection.prepareStatement(Constants.Queries.SEARCH_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, employee_id);
			rs=pstmt.executeQuery();
			if(rs.next())
			{
				return rs.getString(1);
			}
		}catch(Exception e){
			e.printStackTrace();
			connection.rollback();
		}
		return "";
		
	}
}
	


